## Oction Server Installation

#### Deployment Instruction

- Use the Credentials for ENV from .env.example
- Passport need to be installed `php artisan passport:install`
- 

---
###### Daemon Command


###### Scheduler run for every minute
`php artisan schedule:run`

---
---


###### Update Newly added Translated words
`php artisan translation:sync-missing-translation-keys`

###### CORS Publish
`php artisan vendor:publish --tag="cors"`
`php artisan vendor:publish --tag="cors"`
