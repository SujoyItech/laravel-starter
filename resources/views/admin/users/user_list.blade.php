<div class="card-box">
    <h4 class="header-title">{{__('User List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the user list')}}
    </p>

    <div class="table-responsive">
        <table id="user_table" class="table dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Image')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Contact Info')}}</th>
                <th>{{__('Role')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
