<a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
    <img src="{{adminAsset('images/flags/us.jpg')}}" alt="user-image" height="16">
</a>
<div class="dropdown-menu dropdown-menu-right">

    <!-- item-->
    <a href="javascript:void(0);" class="dropdown-item">
        <img src="{{adminAsset('images/flags/germany.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">German</span>
    </a>

    <!-- item-->
    <a href="javascript:void(0);" class="dropdown-item">
        <img src="{{adminAsset('images/flags/italy.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Italian</span>
    </a>

    <!-- item-->
    <a href="javascript:void(0);" class="dropdown-item">
        <img src="{{adminAsset('images/flags/spain.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Spanish</span>
    </a>

    <!-- item-->
    <a href="javascript:void(0);" class="dropdown-item">
        <img src="{{adminAsset('images/flags/russia.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Russian</span>
    </a>

</div>
