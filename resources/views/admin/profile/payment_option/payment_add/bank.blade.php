<div class="form-group">
    <label for="bank_name">{{__('Bank Name')}}</label>
    <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name">
    <div class="valid-feedback">
        {{__('Looks good!')}}
    </div>
</div>
<div class="form-group">
    <label for="tin_number">{{__('Branch Name')}}<span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="branch_name" name="branch_name" placeholder="Branch Name">
    <div class="valid-feedback">
        {{__('Looks good!')}}
    </div>
</div>
<div class="form-group">
    <label for="account_name">{{__('Account Name')}}</label>
    <input type="text" class="form-control" id="account_name" name="account_name" placeholder="Account Name">
    <div class="valid-feedback">
        {{__('Looks good!')}}
    </div>
</div>
<div class="form-group">
    <label for="account_number">{{__('Account Number')}}</label>
    <input type="text" class="form-control" id="account_number" name="account_number" placeholder="Account Number">
</div>

<div class="form-group">
    <label for="routing_number">{{__('Routing Number')}}</label>
    <input type="text" class="form-control" id="routing_number" name="routing_number" placeholder="Account Number">
</div>
